# ui-kit

This is the UI kit composing of all components used by ActionVFX. The design for all the components in this kit comes from the following [design](https://www.figma.com/file/RGjyNVylH2s7izofg7EAkt/%F0%9F%94%A5ActionVFX?node-id=618%3A12978). As a starting point, we will be using [ReactStrap](https://reactstrap.github.io) a library of react bootstrap components. A theme or customization CSS classes will need to be created to match our website design link previously.

**Every** component in ReactStrap is expected to match our design style, but below are some components extracted that are the most important. Please See ReactStrap documentation for these components. The only work required by Onix for these components will be to style them according to our theme.

For other more complex component not in ReactStrap, these will need to be created by Onix either from scratch or using Different components from ReactStrap (implementation can be desided by Onix). We would prefer to not include 2 libraries in our projects, so it would be great if an ActionVFX-UI-KIT package was created that essentially wrapped ReactStrap, and also included some more custom and complex ActionVFX components; however, if it is easier to have 2 packages `reactstrap`, and `actionvfx-ui-components` included in a project, that is also fine.

## Roadmap
- [ ] Accordion
- [ ] Alerts
- [ ] Autocomplete
- [ ] Badge
- [ ] Breadcrumb
- [ ] Buttons
- [ ] Button Group
- [ ] Card
- [ ] Collection Card
- [ ] Dropdowns
- [ ] Form Controls
  - [ ] Checkbox
  - [ ] Fileupload (Input)
  - [ ] Input
  - [ ] Input Group
  - [ ] Input with validation
  - [ ] Select
  - [ ] Textarea
  - [ ] Toggle
- [ ] List
- [ ] Menu
- [ ] Modal
- [ ] Navigation
- [ ] Pagination
- [ ] Popover
- [ ] Progress
- [ ] Spinners
- [ ] Toast Notifications
- [ ] Tooltips
---
- [ ] Blog Hero Header
- [ ] Homepage Header
- [ ] Icons

## Components
### Accordion
![FAQ accordion](docs/images/accordion.jpg)
See [accordion](https://reactstrap.github.io/?path=/docs/components-accordion--accordion) in reactstrap for documenation.

### Alerts
![Alert](docs/images/avfx-alert.png)
The image above displays a dismissable alert from our theme. See [alert](https://reactstrap.github.io/?path=/docs/components-alert--alert) in reactrap for documentation.

### AutoComplete
The autocomplete search suggestion dropdown is a complex custom component that is not available in reactstrap and will have to be created. I will give some specification below. While on our design this is specific to collection search, the component should be customizable enough to allow for use in other context.

![Search Autocomplete Simple](docs/images/search-autocomplete1.png)
![Search Autocomplete Complex](docs/images/search-autocomplete2.png)
This is a combination of a search input field that can be found [here](https://reactstrap.github.io/?path=/docs/components-forms--input-type), and a dropdown that displays some suggestions or results. 

```jsx
<Search>
  <AutoCompleteDropdown>
    <AutoCompleteDropdownSection>
      <h3>Section Title</h3>
      <AutoCompleteItemList>
        <AutoCompleteItem icon={iconComponent} text="some whatever text">
      <AutoCompleteItemList>
    <AutoCompleteDropdownSection>
    
    <AutoCompleteFooter>
      Some stuff for the footer here
    </AutoCompleteFooter>
  </AutoCompleteDropdown>
</Search>
```

#### Events
###### Search
* onFocus
  - default behavior to show dropdown
* onBlur
  - default behavior to hide dropdown
* onChange / onKeyUp
* onEnter / onSubmit

###### AutoCompleteItem
* onClick


### Badge
see [badge](https://reactstrap.github.io/?path=/docs/components-badge--badge) in reactstrap for documentation. 

### Breadcrumb
see [breadcrumb](https://reactstrap.github.io/?path=/docs/components-breadcrumb--breadcrumb)

### Buttons
ReactStrap has all kinds of buttons with different options [here](https://reactstrap.github.io/?path=/docs/components-button--button). They will all need to be style according to our design for their respective type.

### Button Group

### Card
![Card](docs/images/card.png)
![Card Simple](docs/images/simple-card.png)
See ReactStrap [card](https://reactstrap.github.io/?path=/docs/components-card--card). This component only requires theme styling. Please provide an example how how the blog example card would be recreated.

## Collection Card
-- fill me --

### Dropdowns
see [dropdown](https://reactstrap.github.io/?path=/docs/components-dropdown--dropdown)

### Form Controls
all form components to be style according to our themes. No new components needs to be created. see [form](https://reactstrap.github.io/?path=/docs/components-forms--input)
  #### Checkbox
  #### Fileupload (Input)
  ![File Upload](/docs/images/fileupload.png)
  The design of this seems to suggest this would also have "drag and drop" functionality. So keep this in mind. 

  #### Input
  #### Input Group
  see [input group](https://reactstrap.github.io/?path=/docs/components-inputgroup--input-group)
  #### Input with validation
  #### Select
  #### Textarea
  #### Toggle
  Switch Toggle like component. Will need to be created.
  ```jsx
  <ToggleSwitch checkedText="Yes" uncheckedText="No" checked={true} />
  ```

### List
see [list](https://reactstrap.github.io/?path=/docs/components-list--list) and 
see [listgroup](https://reactstrap.github.io/?path=/docs/components-listgroup--list-group)

### Menu
This could be the same as [list-group](https://reactstrap.github.io/?path=/docs/components-modal--modal).

### Modal
see [modal](https://reactstrap.github.io/?path=/docs/components-modal--modal)

### Navigation
![navbar](docs/images/navbar.png)
see both [nav] and [navbar](https://reactstrap.github.io/?path=/docs/components-navbar--navbar) components. They will be crucial in recreating the navbar for our theme. Example of our theme navbar recreated with these components to be provided by Onix. 

### Pagination
![pagination](docs/images/pagination.png)
see [pagination](https://reactstrap.github.io/?path=/docs/components-pagination--pagination) for documentation.

### Popover
![popover](docs/images/popover.png)
see [popover](https://reactstrap.github.io/?path=/docs/components-popover--popover) for documentation

### Progress
see [progress bars](https://reactstrap.github.io/?path=/docs/components-progress--progress) for documentation.

### Spinners
see [loading spinners](https://reactstrap.github.io/?path=/docs/components-spinner--spinner) for documentation. We will only need 1 spinner that matches our design. No need to style for every spinner in this document.

### Toast Notifications
![toast notication](docs/images/toasts.png)
See [toasts](https://reactstrap.github.io/?path=/docs/components-toast--toast) for documentation.

### Tooltips
see [tooltip](https://reactstrap.github.io/?path=/docs/components-tooltip--tooltip) for documentation

## Composite Components
These are bigger components built using many of the other smaller components, or a bit more complex.

### Blog Header
![Blog Header](docs/images/blog-hero-header.png)

### Homepage Header
![Homepage Header](docs/images/homepage-header.png)

## Icons
![Icons](docs/images/icons.png)
There are many custom icons in the design, so we will need to have CSS utilty classes that makes it easier to use those icons. It would also be great if these icons are also part of the react component library, but not a requirement.

